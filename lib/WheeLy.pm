package WheeLy;
use Mojo::Base 'Mojolicious';

sub startup {
  my $self = shift;

  $self->log(Mojo::Log->new);

  my $r = $self->routes;
  $r->get('/')->to('car_log#index');  # _ insted of camel case
}

1;
